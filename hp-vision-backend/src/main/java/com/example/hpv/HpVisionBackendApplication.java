package com.example.hpv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HpVisionBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(HpVisionBackendApplication.class, args);
	}

}
