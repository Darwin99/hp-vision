package com.example.hpv.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.hpv.dao.AllergyDao;
import com.example.hpv.enties.Allergy;

@Service
public class AllergyServices {

	@Autowired
	private AllergyDao allergyDao;
	
	public List<Allergy> getAllAllergys() {
		
		List<Allergy> allergys = new ArrayList<>();
		
		allergyDao.findAll()
		.forEach(allergys::add);
		
		return allergys;
	}

	public  void createAllergy(Allergy p) {
		// TODO Auto-generated method stub
		allergyDao.save(p);
		
		
	}

	public Allergy updateAllergy(Allergy allergy,long id) {
		// TODO Auto-generated method stub
		Optional<Allergy> p = allergyDao.findById(id);
		if(p.isPresent()) {
			return allergyDao.save(allergy);
		}
		return null;
	}

	public boolean deleteAllergy(long id) {
		// TODO Auto-generated method stub
		
		Optional<Allergy> allergy = allergyDao.findById(id);
		if(allergy != null) {
			allergyDao.deleteById(id);
			 return true;
		}
		return false;
	}

}