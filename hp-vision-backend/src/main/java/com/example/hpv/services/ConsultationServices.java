package com.example.hpv.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.hpv.dao.ConsultationDao;
import com.example.hpv.enties.Consultation;

@Service
public class ConsultationServices {
	
	@Autowired
	private ConsultationDao consultationDao;

	public List<Consultation> getAllConsultationsByInTakeInLoad(long inTakeInLoadId) {

		return consultationDao.findAllByInTakeInLoadId(inTakeInLoadId);
		
	}

	public void addConsultion(Consultation consultation) {

		consultationDao.save(consultation);
		
	}

	public boolean updateConsultation(Consultation consultation, long id) {
		Optional<Consultation> consultationOptional;
		
		consultationOptional = consultationDao.findById(id);
		if(consultationOptional.isPresent()) {
			consultationDao.save(consultation);
			return true;
		}
		return false;
	}

}
