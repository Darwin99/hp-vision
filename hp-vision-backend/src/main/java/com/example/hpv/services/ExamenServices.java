package com.example.hpv.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.hpv.dao.ExamenDao;
import com.example.hpv.enties.Examen;
import com.example.hpv.enties.InTakeInLoad;

@Service
public class ExamenServices {

	@Autowired
	private ExamenDao examenDao;

	private final int PAGE_SIZE = 10;

	public java.util.Optional<Examen> getExamById(long id) {
		return examenDao.findById(id);
	}

	public List<Examen> getExamByInTakeInload(long inTakeInLoadId) {

		return examenDao.findByInTakeInLoadId(inTakeInLoadId);
	}

	public Page<Examen> getExamenByNoteBook(long medicalNoteBookId, int page) {

		Pageable paging = PageRequest.of(page, PAGE_SIZE, Sort.by("date").descending());
		return examenDao.findByInTakeInLoadMedicalNoteBookId(medicalNoteBookId, paging);
	}

	public Collection<InTakeInLoad> getExamenByDate(long medicalNoteBookId, Date theDate) {
		List<Examen> recoveredExamens = new ArrayList<Examen>();
		List<Examen> examens = new ArrayList<Examen>();
		HashMap<Long, InTakeInLoad> recoveredInTakeInLoads = new HashMap<Long, InTakeInLoad>();

		examenDao.findByDate(theDate).forEach(recoveredExamens::add);

		recoveredExamens.stream().filter(e -> e.getInTakeInLoad().getMedicalNoteBook().getId() == medicalNoteBookId).forEach(examens::add);

		for (Examen examen : examens) {
			long id = examen.getInTakeInLoad().getId();
			if (!recoveredInTakeInLoads.containsKey(id)) {
				recoveredInTakeInLoads.put(id, examen.getInTakeInLoad());
			}
		}

		return recoveredInTakeInLoads.values();
	}

	public Collection<InTakeInLoad> getExamenByYear(long medicalNoteBookId, int year) {

		List<Examen> recoveredExamens = new ArrayList<Examen>();
		List<Examen> examens = new ArrayList<Examen>();

		String theYear = new String("" + year);
		Date date = null;

		try {
			date = new SimpleDateFormat("yyyy-MM-dd").parse("2021-01-14");
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String TheIntYear = date.toString().split(" ")[5];

		System.out.println("!!!!!!!!!!!!!!!!! Split = " + TheIntYear);

		recoveredExamens = examenDao.findByInTakeInLoadMedicalNoteBookId(medicalNoteBookId).getContent();
		recoveredExamens.stream().filter(e -> e.getDate().toString().contains(theYear)).forEach(examens::add);
		HashMap<Long, InTakeInLoad> recoveredInTakeInLoads = new HashMap<Long, InTakeInLoad>();

		for (Examen examen : examens) {
			long id = examen.getInTakeInLoad().getId();
			if (!recoveredInTakeInLoads.containsKey(id)) {
				recoveredInTakeInLoads.put(id, examen.getInTakeInLoad());
			}
		}

		return recoveredInTakeInLoads.values();

	}

	public void addExamen(Examen examen) {

		examenDao.save(examen);

	}

	public boolean update(long id, Examen examen) {

		Optional<Examen> examenOptional;
		examenOptional = examenDao.findById(id);
		if (examenOptional.isPresent()) {
			examenDao.save(examen);
			return true;
		}
		return false;
	}

	public Collection<InTakeInLoad> getExamensByName(long medicalNoteBookId, String name) {

		List<Examen> recoveredExamens = new ArrayList<Examen>();
		List<Examen> examens = new ArrayList<Examen>();
		HashMap<Long, InTakeInLoad> recoveredInTakeInLoads = new HashMap<Long, InTakeInLoad>();

		examenDao.findByInTakeInLoadMedicalNoteBookId(medicalNoteBookId).forEach(recoveredExamens::add);
		
		recoveredExamens.stream().filter(e -> e.getName().equalsIgnoreCase(name) ).forEach(examens::add);
		
		for (Examen examen : examens) {
			long id = examen.getInTakeInLoad().getId();
			if (!recoveredInTakeInLoads.containsKey(id)) {
				recoveredInTakeInLoads.put(id, examen.getInTakeInLoad());
			}
			
					
		}
		
		return recoveredInTakeInLoads.values();
	}
}
