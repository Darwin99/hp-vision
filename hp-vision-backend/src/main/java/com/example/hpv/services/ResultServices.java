package com.example.hpv.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.hpv.dao.ResultDao;
import com.example.hpv.enties.Result;

@Service
public class ResultServices {
	
	@Autowired
	private ResultDao resultDao;

	public List<Result> getAllResultByExam(long examenId) {
		return resultDao.findByExamenId(examenId);
	}

	public void addResult(Result result) {


		resultDao.save(result);
		
	}

	public boolean update(long id, Result result) {
		Optional<Result> resultOptional = resultDao.findById(id);
		if(resultOptional.isPresent()) {
			resultDao.save(result);
			return true;
		}
		
		return false;
	}

	
	
}
