package com.example.hpv.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.hpv.dao.HospitalDao;
import com.example.hpv.enties.Hospital;

@Service
public class HospitalServices {

	@Autowired
	private HospitalDao hospitalDao;

	public List<Hospital> getAllHospitals() {

//		if (hospitalDao.count() == 0)
//			return null;
		List<Hospital> hospitals = new ArrayList<>();

		hospitalDao.findAll().forEach(hospitals::add);

		return hospitals;
	}

	public void createHospital(Hospital p) {
		// TODO Auto-generated method stub
		hospitalDao.save(p);

	}

	public Hospital updateHospital(Hospital hospital, long id) {
		// TODO Auto-generated method stub
		Optional<Hospital> p = hospitalDao.findById(id);
		if (p.isPresent()) {
			return hospitalDao.save(hospital);
		}
		return null;
	}

	public boolean deleteHospital(long id) {
		// TODO Auto-generated method stub

		Optional<Hospital> hospital = hospitalDao.findById(id);
		if (hospital != null) {
			hospitalDao.deleteById(id);
			return true;
		}
		return false;
	}

}
