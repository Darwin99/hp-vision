package com.example.hpv.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.hpv.dao.PatientDao;
import com.example.hpv.enties.Patient;

@Service
public class PatientServices {

	@Autowired
	private PatientDao patientDao;
	
	public List<Patient> getAllPatients() {
		
		List<Patient> patients = new ArrayList<>();
		
		patientDao.findAll()
		.forEach(patients::add);
		
		return patients;
	}

	public  void createPatient(Patient p) {
		// TODO Auto-generated method stub
		patientDao.save(p);
		
		
	}

	public Patient updatePatient(Patient patient,long id) {
		// TODO Auto-generated method stub
		Optional<Patient> p = patientDao.findById(id);
		if(p.isPresent()) {
			return patientDao.save(patient);
		}
		return null;
	}

	public boolean deletePatient(long id) {
		// TODO Auto-generated method stub
		
		Optional<Patient> patient = patientDao.findById(id);
		if(patient != null) {
			 patientDao.deleteById(id);
			 return true;
		}
		return false;
	}

	public Optional<Patient> getPatientById(long id) {
		return patientDao.findById(id);
	}

	public Optional<Patient> getPatientByUserName(String userName) {
		return patientDao.findByUserName(userName);
	}

}
