package com.example.hpv.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.hpv.dao.MedicalStaffDao;
import com.example.hpv.enties.MedicalStaff;

@Service
public class MedicalStaffServices {
	
	@Autowired
	private MedicalStaffDao medicalStaffDao;
	
	public List<MedicalStaff> getAllMedicalStaffs() {
		
		List<MedicalStaff> medicalStaffs = new ArrayList<>();
		
		medicalStaffDao.findAll()
		.forEach(medicalStaffs::add);
		
		return medicalStaffs;
	}

	public  void createMedicalStaff(MedicalStaff p) {
		// TODO Auto-generated method stub
		medicalStaffDao.save(p);
		
		
	}

	public boolean updateMedicalStaff(MedicalStaff medicalStaff,long id) {
		// TODO Auto-generated method stub
		Optional<MedicalStaff> p = medicalStaffDao.findById(id);
		if(p.isPresent()) {
			 medicalStaffDao.save(medicalStaff);
			 return true;
		}
		return false;
	}

	public boolean deleteMedicalStaff(long id) {
		// TODO Auto-generated method stub
		
		Optional<MedicalStaff> medicalStaff = medicalStaffDao.findById(id);
		if(medicalStaff != null) {
			medicalStaffDao.deleteById(id);
			 return true;
		}
		return false;
	}

	public Optional<MedicalStaff> getMedicalStaffByUserName(String userName) {
	
		return medicalStaffDao.findByUserName(userName);
	}

	public Optional<MedicalStaff> getMedicalStaffByid(long id) {
		
		return medicalStaffDao.findById(id);
		
	}


}
