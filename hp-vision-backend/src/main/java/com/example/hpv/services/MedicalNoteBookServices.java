package com.example.hpv.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.hpv.dao.MedicalNoteBookDao;
import com.example.hpv.enties.MedicalNoteBook;

@Service
public class MedicalNoteBookServices {

	@Autowired
	private MedicalNoteBookDao medicalNoteBookDao;

	@SuppressWarnings("unchecked")
	public List<MedicalNoteBook> getAll(long patientId) {
		// TODO Auto-generated method stub
		@SuppressWarnings("rawtypes")
		List medicalNoteBooks = new ArrayList();
		medicalNoteBookDao.findByPatientId(patientId).forEach(medicalNoteBooks::add);
		return medicalNoteBooks;
	}

	public Optional<MedicalNoteBook> getMedicalNoteBook(long id) {
		return medicalNoteBookDao.findById(id);
	}

	public MedicalNoteBook addMedicalNoteBook(MedicalNoteBook medicalNoteBook) {

		return medicalNoteBookDao.save(medicalNoteBook);

	}

	public boolean updateMedicalNoteBook(MedicalNoteBook medicalNoteBook) {
		Optional<MedicalNoteBook> medicalNoteBookOptional;
		medicalNoteBookOptional = medicalNoteBookDao.findById(medicalNoteBook.getId());
		if(medicalNoteBookOptional.isPresent()) {
			medicalNoteBookDao.save(medicalNoteBook);
			return true;
		}
		
		return false;
		

	}

	public boolean deleteMedicalNoteBook(long id) {
		//we have to verify before if the medical note book exists in data base, after this, we can delete it
		Optional<MedicalNoteBook> medicalNoteBookOptional;
		medicalNoteBookOptional = medicalNoteBookDao.findById(id);
		if(medicalNoteBookOptional.isPresent())
		{
			medicalNoteBookDao.deleteById(id);
			return true;
		}
		
		return false;
		
	}

}
