package com.example.hpv.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.hpv.dao.PrescriptionDao;
import com.example.hpv.enties.Examen;
import com.example.hpv.enties.InTakeInLoad;
import com.example.hpv.enties.Prescription;

@Service
public class PrescriptionServices {

	@Autowired
	private PrescriptionDao prescriptionDao;

	private final int PAGE_SIZE = 10;

	public Optional<Prescription> getPrescriptionById(long id) {
		return prescriptionDao.findById(id);
	}

	public Page<Prescription> getPrescriptionByInTakeInLoad(long inTakeInLoadId, int page) {
		Pageable paging = PageRequest.of(page, PAGE_SIZE);
		return prescriptionDao.findByInTakeInLoadId(inTakeInLoadId, paging);
	}

	public Collection<InTakeInLoad> getPrescriptionBydate(long medicalNoteBookId, Date date) {
		List<Prescription> recoverdPrescriptions = new ArrayList();
		List<Prescription> prescriptions = new ArrayList();
		HashMap<Long, InTakeInLoad> recoveredInTakeInLoads = new HashMap<Long, InTakeInLoad>();

		recoverdPrescriptions = prescriptionDao.findAllByCreationDate(date);

		recoverdPrescriptions.stream().filter(p -> p.getInTakeInLoad().getMedicalNoteBook().getId() == medicalNoteBookId)
				.forEach(prescriptions::add);

		for (Prescription prescription : prescriptions) {
			long id = prescription.getInTakeInLoad().getId();
			if (!recoveredInTakeInLoads.containsKey(id)) {
				recoveredInTakeInLoads.put(id, prescription.getInTakeInLoad());
			}

		}

		return recoveredInTakeInLoads.values();
	}

	public Collection<InTakeInLoad> getPrescriptionByName(long medicalNoteBookId, String name) {
		List<Prescription> recoverdPrescriptions = new ArrayList();
		List<Prescription> prescriptions = new ArrayList();
		HashMap<Long, InTakeInLoad> recoveredInTakeInLoads = new HashMap<Long, InTakeInLoad>();

		prescriptionDao.findByInTakeInLoadMedicalNoteBookId(medicalNoteBookId).forEach(recoverdPrescriptions::add);

		recoverdPrescriptions.stream().filter(p -> p.getName().equalsIgnoreCase(name)).forEach(prescriptions::add);

		for (Prescription prescription : prescriptions) {
			long id = prescription.getInTakeInLoad().getId();
			if (!recoveredInTakeInLoads.containsKey(id)) {
				recoveredInTakeInLoads.put(id, prescription.getInTakeInLoad());
			}

		}

		return recoveredInTakeInLoads.values();
	}

	public List<Prescription> getPrescriptionByNameAndDate(long inTakeInLoadId, String name, Date theDate) {

		List<Prescription> recoverdPrescriptions = new ArrayList();
		List<Prescription> prescriptions = new ArrayList();

		recoverdPrescriptions = prescriptionDao.findAllByCreationDate(theDate);

		recoverdPrescriptions.stream()
				.filter(p -> (p.getInTakeInLoad().getId() == inTakeInLoadId && p.getName().equalsIgnoreCase(name)))
				.forEach(prescriptions::add);

		return prescriptions;

	}

	public void addPrescription(Prescription prescription) {

		prescriptionDao.save(prescription);

	}

	public boolean updatePrescription(long inTakeInLoadId, Prescription prescription) {

		Optional<Prescription> prescriptionOptional;

		prescriptionOptional = prescriptionDao.findById(prescription.getId());
		if (prescriptionOptional.isPresent()) {
			prescriptionDao.save(prescription);
			return true;
		}
		return false;
	}

	public List<Prescription> getPrescriptionByInTakeInLoad(long inTakeInLoadId) {
		return prescriptionDao.findByInTakeInLoadId(inTakeInLoadId);
	}

}
