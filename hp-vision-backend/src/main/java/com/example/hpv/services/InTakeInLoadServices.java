package com.example.hpv.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.hpv.dao.InTakeInLoadDao;
import com.example.hpv.enties.InTakeInLoad;

@Service
public class InTakeInLoadServices {

	@Autowired
	private InTakeInLoadDao inTakeInLoadDao;

	private final int PAGE_SIZE = 10;

	public Optional<InTakeInLoad> getIntakeInLoad(long id) {

		return inTakeInLoadDao.findById(id);

	}

	@SuppressWarnings("unchecked")
	public List<InTakeInLoad> getAll(long medicalNoteBookId) {
		@SuppressWarnings("rawtypes")
		List<InTakeInLoad> inTakeInLoads = new ArrayList();
		inTakeInLoadDao.findAll(Sort.by("beginingDate").descending()).forEach(inTakeInLoads::add);
		return inTakeInLoads;
	}

	public void addInTakeInLoad(InTakeInLoad inTakeInLoad) {

		inTakeInLoadDao.save(inTakeInLoad);

	}

	public boolean update(InTakeInLoad inTakeInLoad, long id) {
		Optional<InTakeInLoad> inTakeInLoadOptional;
		inTakeInLoadOptional = inTakeInLoadDao.findById(id);
		if (inTakeInLoadOptional.isPresent()) {
			inTakeInLoadDao.save(inTakeInLoad);
			return true;
		}
		return false;
	}

	public boolean delete(long id) {

		Optional<InTakeInLoad> inTakeInLoadOptional;
		inTakeInLoadOptional = inTakeInLoadDao.findById(id);
		if (inTakeInLoadOptional.isPresent()) {
			inTakeInLoadDao.deleteById(id);
			return true;
		}
		return false;

	}

	public Page<InTakeInLoad> getAllByMedicalNoteBookByPage(long medicalNoteBookId, int page) {

		Pageable paging = PageRequest.of(page, PAGE_SIZE, Sort.by("beginingDate").descending());
		Page<InTakeInLoad> inTakeInLoadsPage;

		inTakeInLoadsPage = inTakeInLoadDao.findByMedicalNoteBookId(medicalNoteBookId, paging);

		return inTakeInLoadsPage;
	}

	public List<InTakeInLoad> getAllByBeginingDate(long medicalNoteBookId, Date beginingDate) {

		List<InTakeInLoad> recoveredInTakeInLoads = new ArrayList();
		List<InTakeInLoad> inTakeInLoads = new ArrayList();

		recoveredInTakeInLoads = inTakeInLoadDao.findAllByBeginingDate(beginingDate);
		// now we have to filter all in takes in load that has specified the medical
		// note book id
		recoveredInTakeInLoads.stream().filter(i -> i.getMedicalNoteBook().getId() == medicalNoteBookId)
				.forEach(inTakeInLoads::add);

		return inTakeInLoads;
	}

	public List<InTakeInLoad> getAllBetweenDates(long medicalNoteBookId, Date startDate, Date endingDate) {

		List<InTakeInLoad> recoveredInTakeInLoads = new ArrayList();
		List<InTakeInLoad> inTakeInLoads = new ArrayList();

		System.out.println("!!!!!!!!!!!!!!!! StartDate = " + startDate + "\n !!!!!!!!!!!!! EndDate = " + endingDate);

//		System.out.println(
//				"!!!!!!!!!!!!!!!! StartDate = " + theBeginingDate + "\n !!!!!!!!!!!!! EndDate = " + theEndingDate);

		recoveredInTakeInLoads = inTakeInLoadDao.findAllByBeginingDateBetween(startDate, endingDate);

		// now we have to filter all in takes in load that has specified the medical
		// note book id
		recoveredInTakeInLoads.stream().filter(i -> i.getMedicalNoteBook().getId() == medicalNoteBookId)
				.forEach(inTakeInLoads::add);

		return inTakeInLoads;

	}

}
