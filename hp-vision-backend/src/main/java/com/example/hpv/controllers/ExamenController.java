package com.example.hpv.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.hpv.enties.Examen;
import com.example.hpv.enties.InTakeInLoad;
import com.example.hpv.enties.MedicalStaff;
import com.example.hpv.services.ExamenServices;


@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
@RestController
public class ExamenController {

	
	@Autowired
	private ExamenServices examenServices;
	
	@GetMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads/{inTakeInLoadId}/examens/{id}")
	public Optional<Examen> getExamenById(@PathVariable long id){
		return examenServices.getExamById(id);
	}
	
	@GetMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads/{inTakeInLoadId}/examens")
	public List<Examen> getExamByInTakeInLoad(@PathVariable long inTakeInLoadId){
		 
		return examenServices.getExamByInTakeInload(inTakeInLoadId);
	}
	
	@GetMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/examens/page/{page}")
	public Page<Examen> getExamenByNoteBook(@PathVariable long medicalNoteBookId,@PathVariable int page ){
		
		return examenServices.getExamenByNoteBook(medicalNoteBookId,page - 1);
	}
	
	@GetMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/examens/date/{date}")
	public Collection<InTakeInLoad> getExamByDate(@PathVariable long medicalNoteBookId,@PathVariable String date){
			
		Date theDate = null;
		try {
			theDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return examenServices.getExamenByDate(medicalNoteBookId ,theDate);
	}
	
	@GetMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/examens/year/{year}")
	public Collection<InTakeInLoad> getExamenByYear(@PathVariable long medicalNoteBookId, @PathVariable int year){
		
		System.out.println("********************************* Year = " + year);
		return examenServices.getExamenByYear(medicalNoteBookId,year);
	}
	
	@GetMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/examens/name/{name}")
	public Collection<InTakeInLoad> getExamenByName(@PathVariable long medicalNoteBookId, @PathVariable String name){
		return examenServices.getExamensByName(medicalNoteBookId,name);
	}
	
	@PostMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads/{inTakeInLoadId}/examens/medical_staff/{MedicalStaffId}")
	public void addExamen(@RequestBody Examen examen,@PathVariable long MedicalStaffId) {
		examen.setMedicalStaff(new MedicalStaff(MedicalStaffId));
		examenServices.addExamen(examen);
	}
	
	@PutMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads/{inTakeInLoadId}/examens/id")
	public boolean upadteExamen(@PathVariable long id,@RequestBody Examen examen) {
		return examenServices.update(id,examen);
	}
}
