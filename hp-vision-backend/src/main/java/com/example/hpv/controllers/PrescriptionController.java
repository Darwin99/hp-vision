package com.example.hpv.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.hpv.enties.InTakeInLoad;
import com.example.hpv.enties.MedicalStaff;
import com.example.hpv.enties.Prescription;
import com.example.hpv.services.PrescriptionServices;

@RestController
public class PrescriptionController {
	
	@Autowired
	private PrescriptionServices prescriptionServices;

	
	@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
	@GetMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads/{inTakeInLoadId}/prescriptions/{id}")
	public Optional<Prescription> getPresciptioknById(@PathVariable long id) {
		return prescriptionServices.getPrescriptionById(id);
	}
	
	
	@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
	@GetMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads/{inTakeInLoadId}/prescriptions")
	public List<Prescription> getAllPresciptioknByInTakeInLoad(@PathVariable long inTakeInLoadId) {
		return prescriptionServices.getPrescriptionByInTakeInLoad(inTakeInLoadId);
	}
	
	@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
	@GetMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads/{inTakeInLoadId}/prescriptions/page/{page}")
	public Page<Prescription> getPresciptioknByInTakeInLoad(@PathVariable long inTakeInLoadId,@PathVariable int page) {
		return prescriptionServices.getPrescriptionByInTakeInLoad(inTakeInLoadId,page-1);
	}
	
	@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
	@GetMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/prescriptions/date/{date}")
	public Collection<InTakeInLoad> getPresciptioknBydate(@PathVariable long medicalNoteBookId,@PathVariable String date) {
		
		Date theDate = null;
		try {
			
			theDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return prescriptionServices.getPrescriptionBydate(medicalNoteBookId,theDate);
	}
	
	
	@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
	@GetMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/prescriptions/name/{name}")
	public Collection<InTakeInLoad> getPresciptioknByName(@PathVariable long medicalNoteBookId,@PathVariable String name) {
		
		
		return prescriptionServices.getPrescriptionByName(medicalNoteBookId,name);
	}
	
	
	@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
	@GetMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads/{inTakeInLoadId}/prescriptions/name/{name}/date/{date}")
	public List<Prescription> getPresciptioknByNameAndDate(@PathVariable long inTakeInLoadId,@PathVariable String name, @PathVariable String date) {
		
		

		Date theDate = null;
		try {
			
			theDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return prescriptionServices.getPrescriptionByNameAndDate(inTakeInLoadId,name,theDate);
	}
	
	
	@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
	@PostMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads/{inTakeInLoadId}/prescriptions//medical_staff/{medicalStaffId}")
	public void addPrescription(@PathVariable long inTakeInLoadId,@RequestBody Prescription prescription,@PathVariable long medicalStaffId) {
	
		prescription.setMedicalStaff(new MedicalStaff(medicalStaffId));
		prescription.setInTakeInLoad(new InTakeInLoad(inTakeInLoadId));
		prescriptionServices.addPrescription(prescription);
	}
	
	
	@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
	@PutMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads/{inTakeInLoadId}/prescriptions/{id}")
	public boolean updatePrescription(@PathVariable long id,@RequestBody Prescription prescription) {
		return prescriptionServices.updatePrescription(id,prescription);
	}
	
	
	
	
	
}
