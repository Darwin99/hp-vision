package com.example.hpv.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.hpv.enties.MedicalNoteBook;
import com.example.hpv.enties.Patient;
import com.example.hpv.services.MedicalNoteBookServices;

@RestController
public class MedicalNoteBookController {
	
	@Autowired
	private MedicalNoteBookServices medicalNoteBookServices;
	
	
	@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
	@GetMapping("/patients/{patientId}/medical_note_books/{id}")
	public Optional<MedicalNoteBook> getMedicalNoteBook(@PathVariable long patientId,@PathVariable long id ) {
		 
		return medicalNoteBookServices.getMedicalNoteBook(id);
			 
		
				
	}
	
	
	@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
	@GetMapping("/patients/{patientId}/medical_note_books")
	public List<MedicalNoteBook> getAllMedicalNoteBooks(@PathVariable long patientId){
		return medicalNoteBookServices.getAll(patientId); 
	}
	
	
	@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")

	@PostMapping("/patients/{patientId}/medical_note_books")
	public MedicalNoteBook addMedicalNoteBook(@RequestBody MedicalNoteBook medicalNoteBook,@PathVariable long patientId) {
	
		medicalNoteBook.setPatient(new Patient(patientId));
		return medicalNoteBookServices.addMedicalNoteBook(medicalNoteBook);
	}
	
	/*
	 * This method return true if the update has made correctly and false otherwise
	 */
	
	
	@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
	@PutMapping("/patients/{patientId}/medical_note_books/{id}")
	public boolean updateMedicalNoteBook(@RequestBody MedicalNoteBook medicalNoteBook,@PathVariable long patientId) {
		medicalNoteBook.setPatient(new Patient(patientId));
		return medicalNoteBookServices.updateMedicalNoteBook(medicalNoteBook);
	}
	
	
	@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
	@DeleteMapping("/patient/{patientId}/medical_note_books/{id}")
	public boolean deleteMedicalNoteBook(@PathVariable long id) {
		
		return medicalNoteBookServices.deleteMedicalNoteBook(id);
	}
}
