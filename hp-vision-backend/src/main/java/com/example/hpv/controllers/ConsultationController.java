package com.example.hpv.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.hpv.enties.Consultation;
import com.example.hpv.enties.InTakeInLoad;
import com.example.hpv.services.ConsultationServices;

@RestController
public class ConsultationController {

	
	@Autowired
	private ConsultationServices consultationServices;
	
	@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
	@GetMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads/{inTakeInLoadId}/consultations")
	public List<Consultation> getAllConsultationsByInTakeInLoad(@PathVariable long inTakeInLoadId){
		return consultationServices.getAllConsultationsByInTakeInLoad(inTakeInLoadId);
	}
	
	@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
	@PostMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads/{inTakeInLoadId}/consultations")
	public void addConsultation(@RequestBody Consultation consultation,@PathVariable long inTakeInLoadId) {
		consultation.setInTakeInLoad(new InTakeInLoad(inTakeInLoadId));
		 consultationServices.addConsultion(consultation);
	}
	
	@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
	@PutMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads/{inTakeInLoadId}/consultations/{id}")
	public boolean updateConsultation(@RequestBody Consultation consultation,@PathVariable long id) {
		return consultationServices.updateConsultation(consultation,id);
	}
	
}
