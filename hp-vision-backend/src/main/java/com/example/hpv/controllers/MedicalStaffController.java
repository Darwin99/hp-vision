package com.example.hpv.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.hpv.enties.Hospital;
import com.example.hpv.enties.MedicalStaff;
import com.example.hpv.services.MedicalStaffServices;


@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
@RestController
@RequestMapping("hospitals/{hospitalId}/medical_staffs")
public class MedicalStaffController {
	
	@Autowired
	private MedicalStaffServices medicalStaffServices;
	
	@GetMapping
	public List<MedicalStaff> getAllMedicalStaffs() {
		return medicalStaffServices.getAllMedicalStaffs();
	}
	
	
	@GetMapping("/user_name/{userName}")
	public Optional<MedicalStaff> getMedicalStaffByUserName(@PathVariable String userName){
		return medicalStaffServices.getMedicalStaffByUserName(userName);
	}
	
	@GetMapping("/id/{id}")
	public Optional<MedicalStaff> getMedicalStaffByid(@PathVariable long id){
		return medicalStaffServices.getMedicalStaffByid(id);
	}
	
	@PostMapping
	public void saveMedicalStaff(@RequestBody MedicalStaff p,@PathVariable long hospitalId) {
		p.setHospital(new Hospital(hospitalId));
		medicalStaffServices.createMedicalStaff(p);
		
	}
	
	@PutMapping("/{id}")
	public boolean updateMedicalStaff(@RequestBody MedicalStaff medicalStaff, @PathVariable long id){
		return medicalStaffServices.updateMedicalStaff(medicalStaff, id);
		
	}
	
	@DeleteMapping("/{id}")
	public boolean deleteMedicalStaff(@PathVariable long id) {
	
		return medicalStaffServices.deleteMedicalStaff(id);
	}

		
	

}
