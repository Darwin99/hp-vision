package com.example.hpv.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.hpv.enties.Examen;
import com.example.hpv.enties.MedicalStaff;
import com.example.hpv.enties.Result;
import com.example.hpv.services.ResultServices;

@RestController
public class ResultController {

	
	@Autowired
	private ResultServices resultServices;
	
	@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
	@GetMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads/{inTakeInLoadId}/examens/{examenId}/results")
	public List<Result> getALLResults(@PathVariable long examenId) {
		
		
		return resultServices.getAllResultByExam(examenId);
	}
	
	
	@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
	@PostMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads/{inTakeInLoadId}/examens/{examenId}/results/medical_staff/{medicalStaffId}")
	public void addResult(@PathVariable long examenId, @RequestBody Result result,@PathVariable long medicalStaffId ) {
		result.setMedicalStaff(new MedicalStaff(medicalStaffId));
		result.setExamen(new Examen(examenId));
		resultServices.addResult(result);
	}
	
	@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
	
	@PutMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads/{inTakeInLoadId}/examens/{examenId}/results/id")
	public boolean updateResult(@PathVariable long id,@RequestBody Result result) {
		return resultServices.update(id,result);
	}
}