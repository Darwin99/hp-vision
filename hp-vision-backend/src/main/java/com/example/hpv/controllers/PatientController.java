package com.example.hpv.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.hpv.enties.Patient;
import com.example.hpv.services.PatientServices;


@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
@RestController
@RequestMapping("/patients")
public class PatientController {

	@Autowired
	private PatientServices patientServices;
	
	@CrossOrigin(originPatterns = "http://locahost:8080",allowedHeaders = "*")
	@GetMapping
	public List<Patient> getAllPatients() {
		return patientServices.getAllPatients();
	}
	
	
	
	@GetMapping("/{id}")
	public Optional<Patient> getPatientByid(@PathVariable long id){
		return patientServices.getPatientById(id);
	}
	
	@GetMapping("/user_name/{userName}")
	public Optional<Patient> getPatientByUserName(@PathVariable String userName){
		return patientServices.getPatientByUserName(userName);
	}
	
	@PostMapping
	public void savePatient(@RequestBody Patient p) {
		patientServices.createPatient(p);
		
	}
	
	@PutMapping("/{id}")
	public Patient updatePatient(@RequestBody Patient patient, @PathVariable long id){
		Patient p = patientServices.updatePatient(patient, id);
		return p;
	}
	
	@DeleteMapping("/{id}")
	public boolean deletePatient(@PathVariable long id) {
	
		return patientServices.deletePatient(id);
	}

		
	
	
}
