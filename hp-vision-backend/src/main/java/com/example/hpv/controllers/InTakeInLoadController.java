package com.example.hpv.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.hpv.enties.InTakeInLoad;
import com.example.hpv.enties.MedicalNoteBook;
import com.example.hpv.services.InTakeInLoadServices;


@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
@RestController
public class InTakeInLoadController {

	@Autowired
	private InTakeInLoadServices inTakeInloadServices;

	@GetMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads/{id}")
	public Optional<InTakeInLoad> getIntakeInLoad(@PathVariable long id) {
		return inTakeInloadServices.getIntakeInLoad(id);
	}

	@GetMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads")
	public List<InTakeInLoad> getAll(@PathVariable long medicalNoteBookId) {
		return inTakeInloadServices.getAll(medicalNoteBookId);
	}

	@GetMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads/page/{page}")
	public Page<InTakeInLoad> getAllByPage(@PathVariable long medicalNoteBookId, @PathVariable int page) {
		return inTakeInloadServices.getAllByMedicalNoteBookByPage(medicalNoteBookId, page);
	}

	/************************************************
	 * LISTING BY
	 * DATE******************************************************************************
	 * 
	 * /*Here,we will have many case The first case, is that all the beginning date
	 * is specified, : we will return in takes in load which have like beginning
	 * date this particular date The second case is that the two interval date are
	 * specified,
	 */
	
	
	@GetMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads/begining_date/{beginingDate}")
	public List<InTakeInLoad> getAllByBegingDate(@PathVariable long medicalNoteBookId, @PathVariable String beginingDate) {

		Date theBeginingDate = null;
		
			
			try {
				theBeginingDate = new SimpleDateFormat("yyyy-MM-dd").parse(beginingDate);
				
			} catch (ParseException e) {

				e.printStackTrace();
			}
			
			return inTakeInloadServices.getAllByBeginingDate(medicalNoteBookId,theBeginingDate);
		
		
		

	}
	

	@GetMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads/startDate/{startDate}/endingDate/{endindDate}")
	public List<InTakeInLoad> getAllBetweenDates(@PathVariable long medicalNoteBookId, @PathVariable String startDate,
			@PathVariable String endingDate) {

		Date theBeginingDate = null;
		Date theEndingDate = null;
		
		System.out.println("!!!!!!!!!!!!!!!! StartDate = " + startDate+ "\n !!!!!!!!!!!!! EndDate = " + endingDate);
		
		
			try {
				theBeginingDate = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
				theEndingDate = new SimpleDateFormat("yyyy-MM-dd").parse(endingDate);
				
			} catch (ParseException e) {

				e.printStackTrace();
			}
			
			
			System.out.println("!!!!!!!!!!!!!!!! StartDate = " + theBeginingDate+ "\n !!!!!!!!!!!!! EndDate = " + theEndingDate);		
			
		return inTakeInloadServices.getAllBetweenDates(medicalNoteBookId,theBeginingDate,theEndingDate);

	}

	@PostMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads")
	public void addInTakeInLoad(@RequestBody InTakeInLoad inTakeInLoad, @PathVariable long medicalNoteBookId) {
		inTakeInLoad.setMedicalNoteBook(new MedicalNoteBook(medicalNoteBookId));
		inTakeInloadServices.addInTakeInLoad(inTakeInLoad);
	}

	@PutMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads/{id}")
	public boolean update(@RequestBody InTakeInLoad inTakeInLoad, @PathVariable long id) {
		return inTakeInloadServices.update(inTakeInLoad, id);
	}

	@DeleteMapping("/patients/{patientId}/medical_note_books/{medicalNoteBookId}/in_take_in_loads/{id}")
	public boolean delete(@PathVariable long id) {
		return inTakeInloadServices.delete(id);
	}
}
