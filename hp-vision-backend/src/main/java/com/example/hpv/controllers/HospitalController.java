package com.example.hpv.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.hpv.enties.Hospital;
import com.example.hpv.services.HospitalServices;

@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
@RestController
@RequestMapping("/hospitals")
public class HospitalController {
	
	@Autowired
	private HospitalServices hospitalServices;
	
	@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
	@GetMapping
	public List<Hospital> getAllHospitals() {
		return hospitalServices.getAllHospitals();
	}
	
	@PostMapping
	public void saveHospital(@RequestBody Hospital p) {
		hospitalServices.createHospital(p);
		
	}
	
	@PutMapping("/{id}")
	public Hospital updateHospital(@RequestBody Hospital hospital, @PathVariable long id){
		Hospital p = hospitalServices.updateHospital(hospital, id);
		return p;
	}
	
	@DeleteMapping("/{id}")
	public boolean deleteHospital(@PathVariable long id) {
	
		return hospitalServices.deleteHospital(id);
	}

		
	

}
