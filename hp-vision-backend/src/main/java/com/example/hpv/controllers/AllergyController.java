package com.example.hpv.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.hpv.enties.Allergy;
import com.example.hpv.enties.Patient;
import com.example.hpv.services.AllergyServices;


@CrossOrigin(allowedHeaders = "*",exposedHeaders = "*")
@RestController
@RequestMapping("/patients/{patientId}/allergys")
public class AllergyController {

	@Autowired
	private AllergyServices allergyServices;
	
	@GetMapping
	public List<Allergy> getAllAllergys() {
		return allergyServices.getAllAllergys();
	}
	
	@PostMapping
	public void saveAllergy(@RequestBody Allergy p,@PathVariable long patientId) {
		p.setPatient(new Patient(patientId));
		allergyServices.createAllergy(p);
		
	}
	
	@PutMapping("/{id}")
	public Allergy updateAllergy(@RequestBody Allergy patient, @PathVariable long id){
		Allergy p = allergyServices.updateAllergy(patient, id);
		return p;
	}
	
	@DeleteMapping("/{id}")
	public boolean deleteAllergy(@PathVariable long id) {
	
		return allergyServices.deleteAllergy(id);
	}

		
	
	
}
