package com.example.hpv.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;

import com.example.hpv.enties.Patient;

@Repository
public interface PatientDao extends CrudRepository<Patient, Long>{

	Optional<Patient> findByUserName(String userName);

}
