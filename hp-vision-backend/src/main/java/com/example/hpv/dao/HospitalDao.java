package com.example.hpv.dao;

import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;

import com.example.hpv.enties.Hospital;

@Repository
public interface HospitalDao extends CrudRepository<Hospital, Long>{

}
