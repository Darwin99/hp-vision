package com.example.hpv.dao;

import java.util.Optional;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.hpv.enties.MedicalStaff;

@Repository
public interface MedicalStaffDao extends CrudRepository<MedicalStaff, Long> {

	 Optional<MedicalStaff> findByUserName(String userName);

}
