package com.example.hpv.dao;


import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.example.hpv.enties.InTakeInLoad;

@Repository
public interface InTakeInLoadDao extends JpaRepository<InTakeInLoad, Long>{

	List<InTakeInLoad> findAllByMedicalNoteBookId(long medicalNoteBookId);

	Page<InTakeInLoad> findByMedicalNoteBookId(long medicalNoteBookId, Pageable pageable);
	
	List<InTakeInLoad> findAllByBeginingDate(Date beginingDate);

	
	@Query("SELECT * FROM InTakeInLoad i WHERE i.beginingDate=2021-01-12")
	List<InTakeInLoad> findAllByBeginingDateBetween(Date BegininDateStart, Date BeginingDateEnd);

}
