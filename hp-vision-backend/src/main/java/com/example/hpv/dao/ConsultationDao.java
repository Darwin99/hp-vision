package com.example.hpv.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.hpv.enties.Consultation;

@Repository
public interface ConsultationDao extends JpaRepository<Consultation, Long> {

	 List<Consultation> findAllByInTakeInLoadId(long inTakeInLoadId);
	
}
