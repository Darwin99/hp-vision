package com.example.hpv.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.hpv.enties.Prescription;

@Repository
public interface PrescriptionDao extends JpaRepository<Prescription, Long> {

	Page<Prescription> findByInTakeInLoadId(long inTakeInLoadId, Pageable paging);

	List<Prescription> findAllByCreationDate(Date date);

	List<Prescription> findByName(String name);

	List<Prescription> findByInTakeInLoadId(long inTakeInLoadId);

	List<Prescription> findByInTakeInLoadMedicalNoteBookId(long medicalNoteBookId);

}
