package com.example.hpv.dao;

import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;

import com.example.hpv.enties.Allergy;

@Repository
public interface AllergyDao extends CrudRepository<Allergy, Long>{

}
