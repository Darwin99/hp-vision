package com.example.hpv.dao;


import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.hpv.enties.MedicalNoteBook;


@Repository
public interface MedicalNoteBookDao  extends CrudRepository<MedicalNoteBook, Long>{
	List<MedicalNoteBook> findByPatientId(long patientId);
}
