package com.example.hpv.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.hpv.enties.Result;

@Repository
public interface ResultDao extends JpaRepository<Result, Long> {

	List<Result> findByExamenId(long examenId);

}
