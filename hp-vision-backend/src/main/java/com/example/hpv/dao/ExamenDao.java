package com.example.hpv.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.hpv.enties.Examen;

@Repository
public interface ExamenDao extends JpaRepository<Examen,Long> {

	Page<Examen> findByInTakeInLoadId(long inTakeInLoadId, Pageable paging);

	Page<Examen> findByInTakeInLoadMedicalNoteBookId(long medicalNoteBookId, Pageable paging);

	List<Examen> findByDate(Date theDate);

	Slice<Examen> findByInTakeInLoadMedicalNoteBookId(long medicalNoteBookId);

	List<Examen> findByInTakeInLoadId(long inTakeInLoadId);

}
