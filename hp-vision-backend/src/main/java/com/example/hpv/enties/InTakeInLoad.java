package com.example.hpv.enties;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class InTakeInLoad {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	@CreationTimestamp
	private Date beginingDate;

	@Column
	@Temporal(TemporalType.DATE)
	private Date endingDate;

	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	private MedicalNoteBook medicalNoteBook;

	public InTakeInLoad() {

	}

	public InTakeInLoad(long id, Date beginingDate, Date endingDate, MedicalNoteBook medicalNoteBook) {
		super();
		this.id = id;
		this.beginingDate = beginingDate;
		this.endingDate = endingDate;
		this.medicalNoteBook = medicalNoteBook;
	}

	public InTakeInLoad(long id) {

		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getBeginingDate() {
		return beginingDate;
	}

	public void setBeginingDate(Date beginingDate) {
		this.beginingDate = beginingDate;
	}

	public Date getEndingDate() {
		return endingDate;
	}

	public void setEndingDate(Date endingDate) {
		this.endingDate = endingDate;
	}

	public MedicalNoteBook getMedicalNoteBook() {
		return medicalNoteBook;
	}

	public void setMedicalNoteBook(MedicalNoteBook medicalNoteBook) {
		this.medicalNoteBook = medicalNoteBook;
	}

}
