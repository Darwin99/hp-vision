package com.example.hpv.enties;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;


@Entity
public class Examen implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(nullable = false)
	private String name;
	
	@Column 
	@Temporal(TemporalType.DATE)
	@CreationTimestamp
	private Date date;
	
	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	private InTakeInLoad inTakeInLoad;
	
	public MedicalStaff getMedicalStaff() {
		return medicalStaff;
	}

	public void setMedicalStaff(MedicalStaff medicalStaff) {
		this.medicalStaff = medicalStaff;
	}

	@ManyToOne
	private MedicalStaff medicalStaff;
	
	public Examen() {
		
	}
	
	public Examen(long id) {
		this.id = id;
	}

	public Examen(long id, String name, Date date) {
		super();
		this.id = id;
		this.name = name;
		this.date = date;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public InTakeInLoad getInTakeInLoad() {
		return inTakeInLoad;
	}

	public void setInTakeInLoad(InTakeInLoad inTakeInLoad) {
		this.inTakeInLoad = inTakeInLoad;
	}
	
	
}
