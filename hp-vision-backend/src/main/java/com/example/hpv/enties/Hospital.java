package com.example.hpv.enties;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Hospital {
	
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private long id;
		
		@Column(unique = false, nullable = false)
		private String name;
		
		@Column(unique = true, nullable = false)
		private String email;
		
		@Column(unique = true, nullable = true)
		private String tel1;
		
		@Column(unique = true, nullable = false)
		private String tel2;
		
		@Column(unique = true, nullable = true)
		private String POBox;
		
		@Column(unique = true, nullable = true)
		private String fax;
		
		
		public Hospital(long id, String name, String email, String tel1, String tel2, String pOBox, String fax) {
			super();
			this.id = id;
			this.name = name;
			this.email = email;
			this.tel1 = tel1;
			this.tel2 = tel2;
			this.POBox = pOBox;
			this.fax = fax;
		}
		
		public Hospital(long id) {
			super();
			this.id = id;
		}
		
		public Hospital() {
			
		}


		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getTel1() {
			return tel1;
		}

		public void setTel1(String tel1) {
			this.tel1 = tel1;
		}

		public String getTel2() {
			return tel2;
		}

		public void setTel2(String tel2) {
			this.tel2 = tel2;
		}

		public String getPOBox() {
			return POBox;
		}

		public void setPOBox(String pOBox) {
			POBox = pOBox;
		}

		public String getFax() {
			return fax;
		}

		public void setFax(String fax) {
			this.fax = fax;
		}
		
		
		


}
