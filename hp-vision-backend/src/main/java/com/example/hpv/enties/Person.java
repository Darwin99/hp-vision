package com.example.hpv.enties;

import java.io.Serializable;
import java.util.Date;

public class Person implements Serializable {


	protected String name;
	protected String firstName;
	protected Date birthDate;
	protected long CNINumber;
	protected enum sex {
		Masculin,
		Feminie
	}
	
	protected String  image;
	protected enum statut{
		Married,
		Single
	}
	protected String userName;
	protected String password;
	protected enum typ{
		Patient,
		Nurse,
		Doctor
	 }
	protected String email;
	protected String city;
	protected String quarter;
	protected String tel1;
	protected String tel2;
	
	public Person() {
		
	}
	
	public Person(String name, String firstName, Date birthDate, long cNINumber, String userName, String password,
			String email, String city, String tel1) {
		super();
		this.name = name;
		this.firstName = firstName;
		this.birthDate = birthDate;
		CNINumber = cNINumber;
		this.userName = userName;
		this.password = password;
		this.email = email;
		this.city = city;
		this.tel1 = tel1;
	}
	
	public Person(String name, String firstName, Date birthDate, long cNINumber, String image, String userName,
			String password, String email, String city, String quarter, String tel1, String tel2) {
		super();
		this.name = name;
		this.firstName = firstName;
		this.birthDate = birthDate;
		CNINumber = cNINumber;
		this.image = image;
		this.userName = userName;
		this.password = password;
		this.email = email;
		this.city = city;
		this.quarter = quarter;
		this.tel1 = tel1;
		this.tel2 = tel2;
	}

	

	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public long getCNINumber() {
		return CNINumber;
	}

	public void setCNINumber(long cNINumber) {
		CNINumber = cNINumber;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getQuarter() {
		return quarter;
	}

	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}

	public String getTel1() {
		return tel1;
	}

	public void setTel1(String tel1) {
		this.tel1 = tel1;
	}

	public String getTel2() {
		return tel2;
	}

	public void setTel2(String tel2) {
		this.tel2 = tel2;
	}
	
	
	
}
