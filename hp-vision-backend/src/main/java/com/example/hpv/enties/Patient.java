package com.example.hpv.enties;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "patient")
public class Patient {

//	private enum BoodGroup{
//		APlus,
//		BPlus,
//		ABPlus,
//		OPlus,
//		AMinus,
//		BMinus,
//		ABMinus,
//		OMius
//	}
//	
//	
//	
//	private enum Electrophoresis{
//		AA,
//		AS,
//		SS
//	}
//	
//	
//	protected enum Type{
//		Patient,
//		Nurse,
//		Doctor
//	 }
//	
//	protected enum Sex {
//		Masculin,
//		Feminie
//	}
//	
//	
//	protected enum Status{
//		Married,
//		Single
//	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	// The number of near person that we can contact
	@Column(nullable = false)
	private String relativeTel;

	// The associate name of the previous number phone
	@Column(nullable = false)
	private String relativeName;

	private String boodGroup;

	private String electrophoresis;

	@Column(nullable = false)
	protected String name;

	protected String firstName;

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	protected Date birthDate;

	@Column(unique = true)
	protected long CNINumber;

	@Column(unique = true)
	protected String email;

	@Column(nullable = false)
	protected String city;

	@Column(nullable = false)
	protected String quarter;

	@Column(unique = false, nullable = true)
	protected String tel1;
	
	@Column
	protected String tel2;

	@Column(unique = true, nullable = false)
	protected String userName;

	@Column(nullable = false)
	protected String password;

	
	protected String image;

	// this type is related to the patient, medicalStaff
	@Column(nullable = false)
	private String type;

	@Column(nullable = false)
	private String sex;

	@Column(nullable = false)
	private String status;

	public Patient() {

	}

	public Patient(String relativeTel, String relativeName, String boodGroup, String electrophoresis, String name,
			String firstName, Date birthDate, long cNINumber, String email, String city, String quarter, String tel1,
			String tel2, String userName, String password, String image, String type, String sex, String status) {
		super();
		this.relativeTel = relativeTel;
		this.relativeName = relativeName;
		this.boodGroup = boodGroup;
		this.electrophoresis = electrophoresis;

		this.name = name;
		this.firstName = firstName;
		this.birthDate = birthDate;
		CNINumber = cNINumber;
		this.email = email;
		this.city = city;
		this.quarter = quarter;
		this.tel1 = tel1;
		this.tel2 = tel2;
		this.userName = userName;
		this.password = password;
		this.image = image;
		this.type = type;
		this.sex = sex;
		this.status = status;
	}

	public Patient(String relativeTel, String relativeName, String name, Date birthDate, long cNINumber, String city,
			String quarter, String userName, String password, String type, String sex, String status) {
		super();
		this.relativeTel = relativeTel;
		this.relativeName = relativeName;
		this.name = name;
		this.birthDate = birthDate;
		CNINumber = cNINumber;
		this.city = city;
		this.quarter = quarter;
		this.userName = userName;
		this.password = password;
		this.type = type;
		this.sex = sex;
		this.status = status;
	}

	public Patient(String relativeTel, String relativeName, String name, String firstName, Date birthDate,
			long cNINumber, String email, String city, String quarter, String tel1, String userName, String password,
			String image, String type, String sex, String status) {
		super();
		this.relativeTel = relativeTel;
		this.relativeName = relativeName;
		this.name = name;
		this.firstName = firstName;
		this.birthDate = birthDate;
		CNINumber = cNINumber;
		this.email = email;
		this.city = city;
		this.quarter = quarter;
		this.tel1 = tel1;
		this.userName = userName;
		this.password = password;
		this.image = image;
		this.type = type;
		this.sex = sex;
		this.status = status;
	}

	public Patient(long id) {

		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRelativeTel() {
		return relativeTel;
	}

	public void setRelativeTel(String relativeTel) {
		this.relativeTel = relativeTel;
	}

	public String getRelativeName() {
		return relativeName;
	}

	public void setRelativeName(String relativeName) {
		this.relativeName = relativeName;
	}

	public String getBoodGroup() {
		return boodGroup;
	}

	public void setBoodGroup(String boodGroup) {
		this.boodGroup = boodGroup;
	}

	public String getElectrophoresis() {
		return electrophoresis;
	}

	public void setElectrophoresis(String electrophoresis) {
		this.electrophoresis = electrophoresis;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public long getCNINumber() {
		return CNINumber;
	}

	public void setCNINumber(long cNINumber) {
		CNINumber = cNINumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getQuarter() {
		return quarter;
	}

	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}

	public String getTel1() {
		return tel1;
	}

	public void setTel1(String tel1) {
		this.tel1 = tel1;
	}

	public String getTel2() {
		return tel2;
	}

	public void setTel2(String tel2) {
		this.tel2 = tel2;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
