package com.example.hpv.enties;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;


@Entity
public class Consultation implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column
	private String temparature;
	private String weight;
	private String tesion;
	private String synptoms;
	
	@Column
	@Temporal(TemporalType.DATE)
	private Date date;
	
	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	private InTakeInLoad inTakeInLoad;
	
	public Consultation() {
		
	}
	public Consultation(long id, String temparature, String weight, String tesion, String synptoms, Date date,
			InTakeInLoad inTakeInLoad) {
		super();
		this.id = id;
		this.temparature = temparature;
		this.weight = weight;
		this.tesion = tesion;
		this.synptoms = synptoms;
		this.date = date;
		this.inTakeInLoad = inTakeInLoad;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTemparature() {
		return temparature;
	}
	public void setTemparature(String temparature) {
		this.temparature = temparature;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getTesion() {
		return tesion;
	}
	public void setTesion(String tesion) {
		this.tesion = tesion;
	}
	public String getSynptoms() {
		return synptoms;
	}
	public void setSynptoms(String synptoms) {
		this.synptoms = synptoms;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public InTakeInLoad getInTakeInLoad() {
		return inTakeInLoad;
	}
	public void setInTakeInLoad(InTakeInLoad inTakeInLoad) {
		this.inTakeInLoad = inTakeInLoad;
	}
	
	
	
	
	
}
