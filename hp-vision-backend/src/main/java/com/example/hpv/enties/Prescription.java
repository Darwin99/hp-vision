package com.example.hpv.enties;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;



@SuppressWarnings("serial")
@Entity
public class Prescription  implements Serializable{

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private String type;
	
	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	@CreationTimestamp
	private Date creationDate;
	
	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	private InTakeInLoad inTakeInLoad;
	
	@ManyToOne
	private MedicalStaff medicalStaff;
	
	public Prescription() {
		
	}

	public Prescription(long id, String name, String type, Date creationDate) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.creationDate = creationDate;
	}
	
	public Prescription(long id ) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public InTakeInLoad getInTakeInLoad() {
		return inTakeInLoad;
	}

	public void setInTakeInLoad(InTakeInLoad inTakeInLoad) {
		this.inTakeInLoad = inTakeInLoad;
	}

	

	public MedicalStaff getMedicalStaff() {
		return medicalStaff;
	}

	public void setMedicalStaff(MedicalStaff medicalStaff) {
		this.medicalStaff = medicalStaff;
	}
	
	
}
