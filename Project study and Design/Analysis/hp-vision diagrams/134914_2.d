format 213

classinstance 128130 class_ref 135938 // Person
  name ""   xyz 166 159 2000 life_line_z 2000
classinstancecanvas 128258 classinstance_ref 128642 // Person
  xyz 497 147 2000 life_line_z 2000
end
durationcanvas 128642 classinstance_ref 128130 // :Person
  xyzwh 185 271 2010 11 40
end
durationcanvas 128770 classinstance_ref 128258 // Person:Person
  xyzwh 532 271 2010 11 25
end
durationcanvas 129026 classinstance_ref 128130 // :Person
  xyzwh 185 330 2010 11 40
end
durationcanvas 129154 classinstance_ref 128258 // Person:Person
  xyzwh 532 330 2010 11 25
end
msg 128898 synchronous
  from durationcanvas_ref 128642
  to durationcanvas_ref 128770
  yz 271 2015 unspecifiedmsg
  show_full_operations_definition default show_class_of_operation default drawing_language default show_context_mode default
msg 129282 synchronous
  from durationcanvas_ref 129026
  to durationcanvas_ref 129154
  yz 330 2015 unspecifiedmsg
  show_full_operations_definition default show_class_of_operation default drawing_language default show_context_mode default
end
